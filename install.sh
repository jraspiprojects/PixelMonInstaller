#/bin/bash
#Assumes you use curl -sSL https://github.com/jraspiprojects/PixelMonInstaller/raw/master/install.sh | sudo bash
apt-get install java javac whiptail wget

function messageBoxDemo() {
    whiptail --title "Message" --msgbox "Welcome to The PixelMon Installer for Mac! Hit Space To Continue." 8 78
}

function progressGuageDemo() {
    {
        for ((i = 0 ; i <= 100 ; i+=5)); do
            sleep 10.5
            echo $i
        done
    } | whiptail --gauge "Please wait while we are downloading files..." 6 50 0

}

function messageBoxDemo2() {
    whiptail --title "Message" --msgbox "Do NOT Specify a mod in the install prompt. Come back here when you are done." 8 78
}

function messageBoxDemo3() {
    whiptail --title "Message" --msgbox "When you are done, click SPACE to continue." 8 78
}

function InstallWithModFolder(){
  cd mods
  wget https://dl.reforged.gg/2DK4KSh
  whiptail --title "Message" --msgbox "We are done! Click the forge profile in minecraft to start Pixelmon" 8 78
  exit
}

function InstallWithOutModFolder(){
  mkdir mods
  cd mods
  wget https://dl.reforged.gg/2DK4KSh
  whiptail --title "Message" --msgbox "We are done! Click the forge profile in minecraft to start Pixelmon" 8 78
  exit
}


messageBoxDemo

progressGuageDemo

wget https://github.com/jraspiprojects/PixelMonInstaller/raw/master/forge-1.12.2-14.23.3.2655-installer.jar -O forgeinstaller.jar


messageBoxDemo2

java -jar forgeinstaller.jar

messageBoxDemo3

cd $HOME/Library/Application\ Support/

if [ -d "mods" ]; then
  InstallWithModFolder

else
  InstallWithOutModFolder
fi

exit

